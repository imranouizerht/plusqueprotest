<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Movie extends Model
{    use HasFactory;

    protected $table = 'movies';
    protected $fillable = [
        'Id',
        'title',
        'original_language',
        'original_title',
        'overview',
        'poster_path',
        'media_type',
        'popularity',
        'release_date',
        'vote_average',
        'vote_count'
    ];
}
