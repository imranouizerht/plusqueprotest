<?php

namespace App\Console\Commands;

use App\Models\Movie;
use Cassandra\Date;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class ImportMovies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import movies from the API and store it in database';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $client = new Client();
        $url = "https://api.themoviedb.org/3/trending/all/day";
        $headers=['Authorization'=>"Bearer " . env('MOVIE_API')];
        $response = $client->request('GET', $url, ['headers'=>$headers]);
        $movies = json_decode($response->getBody());
        foreach ($movies->results as $movie)
        {
            Movie::create([
                'title'=>$movie->title ?? 'Unknown',
                'original_language'=>$movie->original_language,
                'original_title'=>$movie->original_title ?? 'Unknown',
                'overview'=>$movie->overview,
                'poster_path'=>$movie->poster_path,
                'media_type'=>$movie->media_type,
                'popularity'=>$movie->popularity,
                'release_date'=>$movie->release_date ?? null,
                'vote_average'=>$movie->vote_average,
                'vote_count'=>$movie->vote_count


            ]);
        }
        $this->info("Done !");
    }
}
