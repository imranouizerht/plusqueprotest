<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MovieController extends Controller
{
    public function getMovies(){
        /*$client = new Client();
        $url = "https://api.themoviedb.org/3/trending/all/day";
        $headers = ['Authorization' => 'Bearer ' . env('MOVIE_API')];
        $response = $client->request('GET', $url, ['headers'=>$headers]);
        $movies = json_decode($response->getBody());*/
        $movies = Movie::all();
        return view('Movies/movies', ['movies'=>$movies]);

    }

    public function getMovie(int $movieId)
    {
        $client = new Client();
        $url = "https://api.themoviedb.org/3/movie/" . $movieId;
        $headers = ['Authorization' => 'Bearer ' . env('MOVIE_API')];
        $response = $client->request('GET', $url, ['headers'=>$headers]);
        $movie = json_decode($response->getBody());
        return view('Movies/movie_details', ['movie'=>$movie]);

    }
}
