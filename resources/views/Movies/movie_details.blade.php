<h1>{{$movie->title ?? ''}}</h1>
<h3>"{{$movie->tagline}}"</h3>
<img src="https://image.tmdb.org/t/p/w500/{{$movie->backdrop_path}}" alt="{{$movie->title ?? ''}}">
<p>Categorie:</p>
    <ul>
    @foreach($movie->genres as $genre)
        <li>{{$genre->name}}</li>
    @endforeach
</ul>
Orignal language: {{$movie->original_language}}<br>
Overview : {{$movie->overview}} <br>
Popularity: {{$movie->popularity}}<br>
Production:
<ul>
    @foreach($movie->production_companies as $production)
        <li>{{$production->name}}</li>
    @endforeach
</ul>
Released : {{$movie->release_date ?? 'Soon'}}<br>

Language Availble:
<ul>
    @foreach($movie->spoken_languages as $language)
        <li>{{$language->english_name}}</li>
    @endforeach
</ul>
